
Bu modülün çalışması için;

https://www.drupal.org/project/registry_autoload modülü kurulmalıdır ve aktif edilmeli.
Modüle ait ayarlar bu modül içine girilmiştir.

https://github.com/volkan/iyzipay-payment-api-phpclient sayfasından kütüphane,

sites/all/libraries/ klasörü altına "iyzipay" klasör adıyla indirilmeli.

örnek yapı: "sites/all/libraries/iyzipay/src"